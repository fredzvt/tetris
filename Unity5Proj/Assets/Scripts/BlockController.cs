﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class BlockController : MonoBehaviour
{
    public void SetPosition(int x, int y)
    {
        transform.localPosition = new Vector3(x, y, GameSettings.blockZPosition);
    }

    public void SetColor(Color color)
    {
        var meshRenderer = GetMeshRenderer();
        meshRenderer.material.color = color;
    }

    public Color GetColor()
    {
        var meshRenderer = GetMeshRenderer();
        return meshRenderer.material.color;
    }

    private MeshRenderer GetMeshRenderer()
    {
        var meshRenderer = GetComponent<MeshRenderer>();
        if (meshRenderer == null)
            throw new Exception("meshRenderer is null.");

        return meshRenderer;
    }
}