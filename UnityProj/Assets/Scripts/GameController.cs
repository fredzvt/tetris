﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public GameObject boardPrefab;
    public GameObject piecePrefab;
    public GameObject blockPrefab;

    public Transform cameraPivot;
    public Text uiPointsLabel;
    public Text uiLevelLabel;
    public GameObject uiCentralPanel;
    public Text uiCentralPanelLabel;
    public Text uiCentralPanelButtonLabel;
    public AudioSource audioMusic;
    public AudioSource audioHardDrop;
    public AudioSource audioSoftDrop;
    public AudioSource audioLineCleared;
    public AudioSource audioMove;
    public AudioSource audioRotation;

    private enum States { Menu, Game, Pause, GameOver }
    private States gameState = States.Menu;
    private int level = 1;
    private int points = 0;
    private int pointsByLevel = 1000;
    private GameObject[,] board;
    private BlockController[,] blocks;
    private GameObject boardFolder;
    private GameObject boardBlocksFolder;
    private PieceController currPiece;
    private float lastDropTime = 0f;
    private float lastMoveKeyDown = 0f;
    private float timeToStartRepeatingMove = .2f;
    private float lastRepeatedLateralMove = 0f;
    private float repeatedLateralMoveInterval = .05f;
    private float dropInterval = .5f;
    private float dropIntervalDecreasePerLevel = .05f;
    private float softDropInterval = .1f;
    private int numOfPieceTypes = Enum.GetNames(typeof(PieceType)).Length;
    private System.Random Rnd = new System.Random((int)DateTime.Now.Ticks);
    
    public void Start()
    {
        ValidateBindings();
        InitBoardBlocksFolder();
        InitBoardFolder();
        InstantiateBoard();
        ShowMenu();
    }

    public void Update()
    {
        if (gameState == States.Game)
        {
            if (points > pointsByLevel * level)
            {
                level++;
                dropInterval -= dropIntervalDecreasePerLevel;
            }

            var input = GetPlayerInput();

            UpdateCurrentPieceMovementAndRotation(input);

            if (input.HardDropPiece)
            {
                while (!(IsCurrentPieceReachedBottom() || IsCurrentPieceCollidingWithBoardBlocks()))
                {
                    points += GameSettings.pointsPerHardDropLine;
                    currPiece.MoveDown();
                }

                audioHardDrop.Play();
                FitPieceAndStartNewOne();
            }
            else
            {
                var interval = input.SoftDropPiece ? softDropInterval : dropInterval;
                if (Time.time - lastDropTime > interval)
                {
                    lastDropTime = Time.time;
                    currPiece.MoveDown();

                    if (input.SoftDropPiece)
                    {
                        points += GameSettings.pointsPerSoftDropLine;
                        audioSoftDrop.Play();
                    }

                    if (IsCurrentPieceReachedBottom() || IsCurrentPieceCollidingWithBoardBlocks())
                    {
                        FitPieceAndStartNewOne();
                    }
                }
            }

            var completedLine = GetCompletedLine();
            var multiplier = 1;
            while (completedLine.HasValue)
            {
                points += GameSettings.pointsPerLineCleared * multiplier;
                DestroyLineAndDropAbove(completedLine.Value);
                completedLine = GetCompletedLine();
                multiplier++;
            }

            if (multiplier > 1)
            {
                audioLineCleared.Play();
            }

            uiPointsLabel.text = points.ToString();
            uiLevelLabel.text = level.ToString();

            if (input.Pause)
            {
                ShowPause();
                gameState = States.Pause;
            }

            UpdateCamera();
        }
    }

    public void HandleButtonClick()
    {
        switch (gameState)
        {
            case States.Menu:
            case States.GameOver:
                Reset();
                HideCentralPanel();
                CreateRandomPieceAtTop();
                gameState = States.Game;
                break;

            case States.Pause:
                HideCentralPanel();
                gameState = States.Game;
                break;
        }
    }

    private void UpdateCamera()
    {
        var highestRow = GetHighestRow();
        var heightPercentage = (float)(highestRow * 100) / (float)(GameSettings.BoardHeight - 1);

        var currRotation = cameraPivot.rotation.eulerAngles;
        var newXRotation = ((heightPercentage * 40) / 100) - 20;
        var newRotation = Quaternion.Euler(newXRotation, currRotation.y, currRotation.z);
        var newLerpedAngle = Quaternion.Lerp(cameraPivot.rotation, newRotation, .01f);
        cameraPivot.rotation = newLerpedAngle;

        var currPos = cameraPivot.position;
        var newYPos = ((heightPercentage * 5) / 100) + 9.5f;
        var newPos = new Vector3(currPos.x, newYPos, currPos.z);
        var newLerpedPos = Vector3.Lerp(currPos, newPos, .01f);
        cameraPivot.position = newLerpedPos;
    }

    private int GetHighestRow()
    {
        for (var y = GameSettings.BoardHeight - 1; y >= 0; y--)
        {
            for (var x = 0; x < GameSettings.BoardWidth; x++)
            {
                if (blocks[x, y] != null)
                {
                    return y;
                }
            }
        }

        return 0;
    }

    private void ShowMenu()
    {
        ShowCentralPanel("Tetris", "Start Game");
    }

    private void ShowPause()
    {
        ShowCentralPanel("Pause", "Resume Game");
    }

    private void ShowGameOver()
    {
        ShowCentralPanel("Game Over", "Start New Game");
    }

    private void ShowCentralPanel(string label, string buttonLabel)
    {
        uiCentralPanel.SetActive(true);
        uiCentralPanelLabel.text = label;
        uiCentralPanelButtonLabel.text = buttonLabel;
    }

    private void HideCentralPanel()
    {
        uiCentralPanel.SetActive(false);
    }

    private void Reset()
    {
        points = 0;

        for (var y = 0; y < GameSettings.BoardHeight; y++)
        {
            for (var x = 0; x < GameSettings.BoardWidth; x++)
            {
                if (blocks[x, y] != null)
                {
                    Destroy(blocks[x, y].gameObject);
                    blocks[x, y] = null;
                }
            }
        }
    }

    private void InitBoardBlocksFolder()
    {
        boardBlocksFolder = new GameObject("Board Blocks");
        boardBlocksFolder.transform.position = Vector3.zero;
    }

    private void InitBoardFolder()
    {
        boardFolder = new GameObject("Board");
        boardBlocksFolder.transform.position = Vector3.zero;
    }

    private void InitBlockAt(int x, int y, Color color)
    {
        var blockGO = (GameObject)Instantiate(blockPrefab, new Vector3(x, y, GameSettings.blockZPosition), Quaternion.identity);

        var block = blockGO.GetComponent<BlockController>();
        if (block == null)
            throw new Exception("blockPrefab does not have a BlockController component attached.");

        block.SetPosition(x, y);
        block.SetColor(color);
        blocks[x, y] = block;
    }

    private void FitPieceAndStartNewOne()
    {
        currPiece.MoveUp();

        var outOfBoard = TransformPieceIntoBoardBlocks();

        if (!outOfBoard)
        {
            CreateRandomPieceAtTop();
        }
        else
        {
            ShowGameOver();
            gameState = States.GameOver;
        }
    }

    private void ValidateBindings()
    {
        if (cameraPivot == null) throw new Exception("cameraPivot is null.");
        if (boardPrefab == null) throw new Exception("boardPrefab is null.");
        if (piecePrefab == null) throw new Exception("piecePrefab is null.");
        if (uiPointsLabel == null) throw new Exception("pointsLabel is null.");
        if (uiLevelLabel == null) throw new Exception("uiLevelLabel is null.");
        if (uiCentralPanel == null) throw new Exception("uiCentralPanel is null.");
        if (uiCentralPanelLabel == null) throw new Exception("uiCentralPanelLabel is null.");
        if (uiCentralPanelButtonLabel == null) throw new Exception("uiCentralPanelButtonLabel is null.");        
        if (audioMusic == null) throw new Exception("Music is null.");
        if (audioHardDrop == null) throw new Exception("HardDrop is null.");
        if (audioSoftDrop == null) throw new Exception("SoftDrop is null.");
        if (audioLineCleared == null) throw new Exception("LineCleared is null.");
        if (audioMove == null) throw new Exception("Move is null.");
        if (audioRotation == null) throw new Exception("Rotation is null.");
    }

    private void InstantiateBoard()
    {
        board = new GameObject[GameSettings.BoardWidth, GameSettings.BoardHeight];
        blocks = new BlockController[GameSettings.BoardWidth, GameSettings.BoardHeight];

        for (var y = 0; y < GameSettings.BoardHeight; y++)
        {
            for (var x = 0; x < GameSettings.BoardWidth; x++)
            {
                var pos = new Vector3(x, y, GameSettings.boardZPosition);
                var obj = (GameObject)Instantiate(boardPrefab, pos, Quaternion.identity);
                obj.transform.parent = boardFolder.transform;
                board[x, y] = obj;
            }
        }
    }

    private PlayerInput GetPlayerInput()
    {
        var input = new PlayerInput();

        input.Pause = Input.GetKeyDown(GameSettings.pauseKey);
        input.SoftDropPiece = Input.GetKey(GameSettings.playerInputSoftDropPiece);
        input.HardDropPiece = Input.GetKeyDown(GameSettings.playerInputHardDropPiece);

        if (!input.HardDropPiece)
        {
            input.RotateLeft = Input.GetKeyDown(GameSettings.playerInputRotateLeft);
            input.RotateRight = Input.GetKeyDown(GameSettings.playerInputRotateRight);

            if (Input.GetKeyDown(GameSettings.playerInputMoveLeft))
            {
                lastMoveKeyDown = Time.time;
                input.MoveLeft = true;
            }
            else if (Input.GetKey(GameSettings.playerInputMoveLeft))
            {
                if (Time.time - lastMoveKeyDown > timeToStartRepeatingMove)
                {
                    if (Time.time - lastRepeatedLateralMove > repeatedLateralMoveInterval)
                    {
                        lastRepeatedLateralMove = Time.time;
                        input.MoveLeft = true;
                    }
                }
            }

            if (Input.GetKeyDown(GameSettings.playerInputMoveRight))
            {
                lastMoveKeyDown = Time.time;
                input.MoveRight = true;
            }
            else if (Input.GetKey(GameSettings.playerInputMoveRight))
            {
                if (Time.time - lastMoveKeyDown > timeToStartRepeatingMove)
                {
                    if (Time.time - lastRepeatedLateralMove > repeatedLateralMoveInterval)
                    {
                        lastRepeatedLateralMove = Time.time;
                        input.MoveRight = true;
                    }
                }
            }
        }

        return input;
    }

    private void CreateRandomPieceAtTop()
    {
        var pieceGO = (GameObject)Instantiate(piecePrefab, Vector3.zero, Quaternion.identity);
        var piece = pieceGO.GetComponent<PieceController>();
        if (piece == null)
            throw new Exception("piecePrefab does not have a PieceController component attached.");

        piece.InitPiece((PieceType)Rnd.Next(0, numOfPieceTypes));

        var xPos = (GameSettings.BoardWidth / 2) - (piece.MatrixSize / 2);
        var yPos = GameSettings.BoardHeight;
        piece.SetPosition(xPos, yPos);

        currPiece = piece;
    }

    private void UpdateCurrentPieceMovementAndRotation(PlayerInput input)
    {
        if (input.MoveLeft && !input.MoveRight)
        {
            currPiece.MoveLeft();

            if (IsCurrentPieceReachedBottom() ||
                IsCurrentPieceHorizontallyOutOfBoard() ||
                IsCurrentPieceCollidingWithBoardBlocks())
            {
                // Cancel movement
                currPiece.MoveRight();
            }
            else
            {
                audioMove.Play();
            }
        }

        if (input.MoveRight && !input.MoveLeft)
        {
            currPiece.MoveRight();

            if (IsCurrentPieceReachedBottom() ||
                IsCurrentPieceHorizontallyOutOfBoard() ||
                IsCurrentPieceCollidingWithBoardBlocks())
            {
                // Cancel movement
                currPiece.MoveLeft();
            }
            else
            {
                audioMove.Play();
            }
        }

        if (input.RotateLeft && !input.RotateRight)
        {
            currPiece.RotateLeft();

            if (IsCurrentPieceReachedBottom() ||
                IsCurrentPieceHorizontallyOutOfBoard() ||
                IsCurrentPieceCollidingWithBoardBlocks())
            {
                // Cancel rotation
                currPiece.RotateRight();
            }
            else
            {
                audioRotation.Play();
            }
        }

        if (input.RotateRight && !input.RotateLeft)
        {
            currPiece.RotateRight();

            if (IsCurrentPieceReachedBottom() ||
                IsCurrentPieceHorizontallyOutOfBoard() ||
                IsCurrentPieceCollidingWithBoardBlocks())
            {
                // Cancel rotation
                currPiece.RotateLeft();
            }
            else
            {
                audioRotation.Play();
            }
        }
    }

    private bool IsCurrentPieceHorizontallyOutOfBoard()
    {
        for (var y = 0; y < currPiece.MatrixSize; y++)
        {
            for (var x = 0; x < currPiece.MatrixSize; x++)
            {
                var block = currPiece.blocks[x, y];

                if (block != null)
                {
                    if (block.transform.position.x < 0)
                    {
                        return true;
                    }

                    if (block.transform.position.x > GameSettings.BoardWidth - 1)
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private bool IsCurrentPieceCollidingWithBoardBlocks()
    {
        for (var y = 0; y < currPiece.MatrixSize; y++)
        {
            for (var x = 0; x < currPiece.MatrixSize; x++)
            {
                var pieceBlock = currPiece.blocks[x, y];
                if (pieceBlock != null)
                {
                    var boardBlockX = (int)pieceBlock.transform.position.x;
                    var boardBlockY = (int)pieceBlock.transform.position.y;

                    if (boardBlockX < GameSettings.BoardWidth &&
                        boardBlockY < GameSettings.BoardHeight)
                    {
                        var boardBlock = blocks[boardBlockX, boardBlockY];

                        if (boardBlock != null)
                        {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    private bool IsCurrentPieceReachedBottom()
    {
        for (var y = 0; y < currPiece.MatrixSize; y++)
        {
            for (var x = 0; x < currPiece.MatrixSize; x++)
            {
                var pieceBlock = currPiece.blocks[x, y];

                if (pieceBlock != null && 
                    pieceBlock.transform.position.y < 0)
                {
                    return true;
                }
            }
        }

        return false;
    }

    private bool TransformPieceIntoBoardBlocks()
    {
        var outOfBoard = false;

        for (var y = 0; y < currPiece.MatrixSize; y++)
        {
            for (var x = 0; x < currPiece.MatrixSize; x++)
            {
                var block = currPiece.blocks[x, y];
                if (block != null)
                {
                    var bx = (int)block.transform.position.x;
                    var by = (int)block.transform.position.y;

                    if (by < GameSettings.BoardHeight)
                    {
                        blocks[bx, by] = block;
                        block.transform.parent = boardBlocksFolder.transform;
                    }
                    else
                    {
                        outOfBoard = true;
                        Destroy(block);
                    }
                }
            }
        }

        Destroy(currPiece.gameObject);
        currPiece = null;
        return outOfBoard;
    }

    private int? GetCompletedLine()
    {
        for (var y = 0; y < GameSettings.BoardHeight; y++)
        {
            var completed = true;

            for (var x = 0; x < GameSettings.BoardWidth; x++)
            {
                if (blocks[x, y] == null)
                {
                    completed = false;
                    break;
                }
            }

            if (completed)
            {
                return y;
            }
        }

        return null;
    }

    private void DestroyLineAndDropAbove(int y)
    {
        for (var x = 0; x < GameSettings.BoardWidth; x++)
        {
            Destroy(blocks[x, y].gameObject);
            blocks[x, y] = null;
        }

        for (var by = y + 1; by < GameSettings.BoardHeight; by++)
        {
            for (var x = 0; x < GameSettings.BoardWidth; x++)
            {
                if (blocks[x, by] != null)
                {
                    blocks[x, by - 1] = blocks[x, by];
                    blocks[x, by].SetPosition(x, by - 1);
                    blocks[x, by] = null;
                }
            }
        }
    }
}