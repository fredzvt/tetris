﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput
{
    public bool Pause { get; set; }
    public bool RotateLeft { get; set; }
    public bool RotateRight { get; set; }
    public bool MoveLeft { get; set; }
    public bool MoveRight { get; set; }
    public bool HardDropPiece { get; set; }
    public bool SoftDropPiece { get; set; }
}