﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PieceType
{
    Line, 
    Square, 
    LBlock, 
    ReverseLBlock,
    TBlock,
    Squiggly,
    ReverseSquiggly
}

public class PieceController : MonoBehaviour
{
    public GameObject blockPrefab;
    public int X { get { return (int)transform.position.x; } }
    public int Y { get { return (int)transform.position.y; } }
    public int MatrixSize { get { return this.blocks.GetLength(0); } }
    public BlockController[,] blocks;

    public void Start()
    {
        ValidateBindings();
    }

    public void InitPiece(PieceType type)
    {
        switch (type)
        {
            case PieceType.Line:
                InstantiateBlocksSquareMatrix(4);
                InitBlockAt(2, 0, Color.red);
                InitBlockAt(2, 1, Color.red);
                InitBlockAt(2, 2, Color.red);
                InitBlockAt(2, 3, Color.red);
                break;

            case PieceType.Square:
                InstantiateBlocksSquareMatrix(2);
                InitBlockAt(0, 0, Color.blue);
                InitBlockAt(1, 1, Color.blue);
                InitBlockAt(0, 1, Color.blue);
                InitBlockAt(1, 0, Color.blue);
                break;

            case PieceType.LBlock:
                InstantiateBlocksSquareMatrix(3);
                InitBlockAt(1, 0, Color.green);
                InitBlockAt(1, 1, Color.green);
                InitBlockAt(1, 2, Color.green);
                InitBlockAt(2, 0, Color.green);
                break;

            case PieceType.ReverseLBlock:
                InstantiateBlocksSquareMatrix(3);
                InitBlockAt(1, 0, Color.cyan);
                InitBlockAt(1, 1, Color.cyan);
                InitBlockAt(1, 2, Color.cyan);
                InitBlockAt(0, 0, Color.cyan);
                break;

            case PieceType.TBlock:
                InstantiateBlocksSquareMatrix(3);
                InitBlockAt(1, 0, Color.magenta);
                InitBlockAt(0, 1, Color.magenta);
                InitBlockAt(1, 1, Color.magenta);
                InitBlockAt(2, 1, Color.magenta);
                break;

            case PieceType.Squiggly:
                InstantiateBlocksSquareMatrix(3);
                InitBlockAt(1, 0, Color.yellow);
                InitBlockAt(0, 1, Color.yellow);
                InitBlockAt(1, 1, Color.yellow);
                InitBlockAt(2, 0, Color.yellow);
                break;

            case PieceType.ReverseSquiggly:
                InstantiateBlocksSquareMatrix(3);
                InitBlockAt(1, 0, Color.gray);
                InitBlockAt(0, 0, Color.gray);
                InitBlockAt(1, 1, Color.gray);
                InitBlockAt(2, 1, Color.gray);
                break;

            default:
                throw new Exception("Unexpected piece type.");
        }
    }

    public void MoveLeft()
    {
        SetPosition(X - 1, Y);
    }

    public void MoveRight()
    {
        SetPosition(X + 1, Y);
    }

    public void MoveDown()
    {
        SetPosition(X, Y - 1);
    }

    public void MoveUp()
    {
        SetPosition(X, Y + 1);
    }

    public void SetPosition(int x, int y)
    {
        transform.position = new Vector3(x, y, 0);
    }

    public void RotateLeft()
    {
        Transpose();
        FlipHorizontally();
    }

    public void RotateRight()
    {
        Transpose();
        FlipVertically();
    }
    
    private void InstantiateBlocksSquareMatrix(int size)
    {
        blocks = new BlockController[size, size];
    }

    private void ValidateBindings()
    {
        if (blockPrefab == null) throw new Exception("blockPrefab is null.");
    }

    private void InitBlockAt(int x, int y, Color color)
    {
        var blockGO = (GameObject)Instantiate(blockPrefab, this.transform.position, Quaternion.identity);
        blockGO.transform.parent = this.transform;

        var block = blockGO.GetComponent<BlockController>();
        if (block == null)
            throw new Exception("blockPrefab does not have a BlockController component attached.");

        block.SetPosition(x, y);
        block.SetColor(color);
        blocks[x, y] = block;
    }

    private void Transpose()
    {
        var d = blocks.GetLength(0);

        for (var x = 0; x <= d - 2; x++)
            for (var y = x + 1; y <= d - 1; y++)
                ChangeBlocks(x, y, y, x);
    }

    private void FlipHorizontally()
    {
        var d = blocks.GetLength(0);
        var half = (int)(d / 2);

        for (var i = 0; i < half; i++)
        {
            var x1 = i;
            var x2 = (d - 1) - i;

            for (var y = 0; y < d; y++)
                ChangeBlocks(x1, y, x2, y);
        }
    }

    private void FlipVertically()
    {
        var d = blocks.GetLength(0);
        var half = (int)(d / 2);

        for (var i = 0; i < half; i++)
        {
            var y1 = i;
            var y2 = (d - 1) - i;

            for (var x = 0; x < d; x++)
                ChangeBlocks(x, y1, x, y2);
        }
    }

    private void ChangeBlocks(int x1, int y1, int x2, int y2)
    {
        var b1 = blocks[x1, y1];
        var b2 = blocks[x2, y2];

        blocks[x1, y1] = b2;
        blocks[x2, y2] = b1;

        if (b1 != null)
            b1.SetPosition(x2, y2);

        if (b2 != null)
            b2.SetPosition(x1, y1);
    }
}