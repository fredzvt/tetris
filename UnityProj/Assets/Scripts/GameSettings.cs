﻿using System;
using UnityEngine;

public static class GameSettings
{
    public static int BoardWidth = 10;
    public static int BoardHeight = 25;
    public static float boardZPosition = .6f;
    public static float blockZPosition = 0;

    public static KeyCode pauseKey = KeyCode.Escape;
    public static KeyCode playerInputRotateLeft = KeyCode.Z;
    public static KeyCode playerInputRotateRight = KeyCode.UpArrow;
    public static KeyCode playerInputHardDropPiece = KeyCode.Space;
    public static KeyCode playerInputSoftDropPiece = KeyCode.DownArrow;
    public static KeyCode playerInputMoveLeft = KeyCode.LeftArrow;
    public static KeyCode playerInputMoveRight = KeyCode.RightArrow;

    public static int pointsPerSoftDropLine = 1;
    public static int pointsPerHardDropLine = 2;
    public static int pointsPerLineCleared = 10;
}